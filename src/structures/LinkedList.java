package structures;

/**
 *  A class to provide the basic methods of a singly linked list.
 *  Note that it is simplified in that it does not implement or extend
 *  List, Collection, etc.  It is for learning the basics of Linked
 *  Lists.
 *
 *
 * extended till friday 
 *you could use Linked List for stack
 *
 *
 *  @author Daniel Plante
 *  @version 1.0   2 March 2002
 *  @version 1.1   19 November 2013
 */
public class LinkedList
{
	//	initialize head and size 
	
    private Node myHead;
    private int mySize;
    
    
    
   
    public LinkedList()
    {
    	/**
    	 * Default constructor that creates an empty linked list
    	 * 
    	 *  <pre>
    	 *  pre:  the linked list is empty
    	 *  post: the linked list is empty
    	 *  </pre>
    	 */
    	
        myHead = null;
        mySize =0;
    }
    
    
    public LinkedList(Object datum)
    {
    	/**
         *  Constructor that creates a new linked list with a single 
         *  node storing the object passed in
         *
         *  <pre>
         *  pre:  myHead points to null (the linked list is empty)
         *  post: myHead points to the only node in the linked list,
         *        that node holding the object passed in
         *  </pre>
         *
         *  @param datum an object to be inserted at the head of the
         *         linked list
         */
    	
        myHead = new Node(datum);
        myHead.setNext(null);
        mySize = 1;
    }
    
   
    private void addFirst(Node node)
    {
    	/**
         *  Adds a node to the head of the linked list; the special
         *  condition of an empty linked list is handled without
         *  special treatment since if myHead points to null, that
         *  simply becomes the next node in the list, immediately
         *  following the new entered node at the head of the list
         *
         *  <pre>
         *  pre:  the linked list may be empty or contain one or
         *        more nodes
         *  post: the linked list contains one more node that has
         *        been added to the beginning of the list
         *  </pre>
         *
         *  @param node the node to be entered
         */
    	
        node.setNext(myHead);
        myHead = node;
        mySize ++;
    }
    
    
    public void addFirst(Object datum)
    {
    	
    	/**
         *  Adds a node to the head of the linked list; the special
         *  condition of an empty linked list is handled without
         *  special treatment since if myHead points to null, that
         *  simply becomes the next node in the list, immediately
         *  following the new entered node at the head of the list
         *
         *  <pre>
         *  pre:  the linked list may be empty or contain one or
         *        more nodes
         *  post: the linked list contains one more node that has
         *        been added to the beginning of the list
         *  </pre>
         *
         *  @param datum the object used to creat a new node to be 
         *         entered at the head of the list
         */
    	
        Node node;
        
        node = new Node(datum);
        this.addFirst(node);
    }
    
   
    private void addLast(Node node)
    {
    	
    	 /**
         *  Adds a node to the tail of the linked list; the special
         *  condition of an empty linked list is handled separately
         *
         *  <pre>
         *  pre:  the linked list may be empty or contain one or
         *        more nodes
         *  post: the linked list contains one more node that has
         *        been added to the end of the list
         *  </pre>
         *
         *  @param node the node to be entered
         */
    	
    	
  
    	
        Node lastNode;
        
        if(myHead==null)
        	
        {
            this.addFirst(node);
        }
        else
        {
            lastNode = this.getPrevious(null);
            lastNode.setNext(node);
            node.setNext(null);
        }
    }
    
    
    public void addLast(Object datum)
    {
    	
    	/**
         *  Adds a node to the tail of the linked list; the special
         *  condition of an empty linked list is handled separately
         *
         *  <pre>
         *  pre:  the linked list may be empty or contain one or
         *        more nodes
         *  post: the linked list contains one more node that has
         *        been added to the end of the list
         *  </pre>
         *
         *  @param datum the object used to create a new node to be 
         *         entered at the tail of the list
         */
    	
    	
        Node node;
        
        node = new Node(datum);
        this.addLast(node);
    }
    
    
    private boolean remove(Node node)
    {
    	
    	/**
         *  Deletes a node from the list if it is there
         *
         *  <pre>
         *  pre:  the list has 0 or more nodes
         *  post: if the node to be deleted is in the list,
         *        the node no longer exists in the list; the
         *        node previous to the node to be deleted now
         *        points to the node following the deleted node
         *  </pre>
         *
         *  @param node the node to be deleted from the list
         *
         *  @return boolean indicating whether or not the node
         *          was deleted
         */
    	
    	
    	if (node == null) return false;
    	
    	
    	Node myNode = this.findNode(node.getData());
    	
    	if (myNode == null) return false;
    	
    	
    	Node prev = this.getPrevious(myNode);
    	Node next = myNode.getNext();
    	
    	if (prev == null)
    	{
    		if (next == null)
    		{
    			myHead = null;
    			mySize--;
    			return true;
    		}
    		
    		else
    		{
    			myHead.setData(next.getData());
    			mySize--;
    			return true;
    		}
    	}
    	
    	prev.setNext(next);
    	mySize --;
    	return true;
    }
    
    
    public boolean remove(Object datum)
    {
    	
    	/**
         *  Deletes a node from the list if it is there
         *
         *  <pre>
         *  pre:  the list has 0 or more nodes
         *  post: if the node to be deleted is in the list,
         *        the node no longer exists in the list; the
         *        node previous to the node to be deleted now
         *        points to the node following the deleted node
         *  </pre>
         *
         *  @param datum the object to be deleted from the list
         *
         *  @return boolean indicating whether or not the node
         *          was deleted
         */
    	
    	
       Node myNode = findNode(datum);
       
       return this.remove(myNode);
    }
    
    /**
     *  Find a node in the list with the same data as that passed in 
     *
     *  <pre>
     *  pre:  the list has 0 or more nodes
     *  post: list is unchanged
     *  </pre>
     *
     *  @param datum the object for which a node is to be found 
     *         in the list
     *
     *  @return null if a node with the given object datum is not in
     *          the list, or the node if it does
     */
    private Node findNode(Object datum)
    {
        Node currentNode;
        Object currentDatum;
        
        currentNode = myHead;
        currentDatum = null;
        
        while(currentNode != null)
        {
            currentDatum = currentNode.getData();
            if(currentDatum.equals(datum))
            {
                return currentNode;
            }
            currentNode = currentNode.getNext();
        }
        return null;
    }
    
    
    public boolean contains(Object datum)
    {
    	
    	/**
         *  Determine if a node exists in the list with the same 
         *  data as that passed in 
         *
         *  <pre>
         *  pre:  the list has 0 or more nodes
         *  post: list is unchanged
         *  </pre>
         *
         *  @param datum the object for which a node is to be found 
         *         in the list
         *
         *  @return false if a node with the given object datum is not in
         *          the list, or true if it does
         */
    	
    	
        Node containsNode = findNode (datum);
        
        if (containsNode != null)
        {
        	return true;
        }
        return false;
    }
    
    
    private Node getPrevious(Node node)
    {
    	
    	/**
         *  Determines the node that resides one closer to the
         *  head of the list than the node passed in
         *
         *  <pre>
         *  pre:  the list has 0 or more nodes
         *  post: the list is unchanged
         *  </pre>
         *
         *  @param node the node whose predecessor is being looked for
         *
         *  @return the node that resides one closer to the head of the
         *          list than the node passed in
         */
    	
        Node currentNode;
        
        currentNode = myHead;
        
        if(currentNode.equals(node))
        {
            return null;
        }
        
        while(currentNode!=null && !currentNode.getNext().equals(node))
        {
            currentNode = currentNode.getNext();
        }
        
        return currentNode;
    }
    
    
    private boolean insertBefore(Node node, Node beforeNode)
    {
    	
    	/**
         *  A new node is entered into the list immediately before
         *  the designated node
         *
         *  <pre>
         *  pre:  the list may have 0 or more nodes in it
         *  post: if the beforeNode is not in the list, no change
         *        takes place to the list; otherwise, the new
         *        node is entered in the appropriate place
         *  </pre>
         *
         *  @param node the node to be entered into the list
         *  @param beforeNode the node before which the new node
         *         is to be entered
         *
         *  @return boolean designating if the node was or was not
         *          entered into list
         */
    	
    	//node = new Node();
    	
        if (beforeNode == null)
        {
        	mySize ++;
        	this.addFirst(node);
        	
        	return true;
        }
        else 
        {
        	mySize++;
        	Node nextNode = beforeNode.getNext();
        	
        	beforeNode.setNext(node);
        	
        	node.setNext(nextNode);
        	
        	return true;
        }
        
    }
    
    
    public boolean insertBefore(Object datum, Object beforeDatum)
    {
    	
    	/**
         *  A new node with , need to still datum is entered into the list immediately
         *  before the node with beforeDatum, the designated node
         *
         *  <pre>
         *  pre:  the list may have 0 or more nodes in it
         *  post: if the node with beforeDatum is not in the list, 
         *        no change takes place to the list; otherwise, a new
         *        node is entered in the appropriate place with the 
         *        object datum
         *  </pre>
         *
         *  @param datum the object used to create the new node 
         *         to be entered into the list
         *  @param beforeDatum the datum of the node before which the 
         *         new node is to be entered
         *
         *  @return boolean designating if the node was or was not
         *          entered
         */
    	
        Node node = new Node(datum);
        Node prev = findNode(beforeDatum);
        
        return insertBefore(node,prev);
        
        
    }
    
    
    public String toString()
    {
    	
    	/**
         *  print the list by converting the objects in the list
         *  to their string representations
         *
         *  <pre>
         *  pre:  the list has 0 or more elements
         *  post: no change to the list
         *  </pre>
         */
    	
    	
        String string;
        Node currentNode;
        
        currentNode = myHead;
        
        string = "head ->";
        
        while(currentNode!=null)
        {
            string += currentNode.getData().toString()+ " -> ";
            currentNode = currentNode.getNext();
        }
        string += "|||";
        return string;
    }

    // ALSO!  Comment and implement the following methods.
    // !!!

    public int indexOf(Object o)
    {
        /**
         * tells you which index of the object from the node it is contained in
         */
    	
    	if(o == null) return -1;
    	
    	if(!contains(o)) return -1;
    	
    	else {
    		Node node = findNode(o);
    		// index of the head is 0 since head is the first node 
    		
    		Node myNode = myHead;
    		
    		int myIndex = 0;
    		
    		//continues to get the next index 
    		while(myNode != node)
    		{
    			myIndex ++;
    			myNode = myNode.getNext();
    			
    			
    		} return myIndex;
    	}
    }

    public Object removeFirst()
    {
    	//let's say we are storing a person's name in the nodes 
    	//helps me do it if I imagine data inside 
    	
    	Object name = this.getFirst();
    	Node node = new Node (name);
    	Object wasRemoved = getFirst();
    	
    	
    	if(name == null && node == null && wasRemoved == null) return null;
    	
    	
    	remove(myHead);
    	
    	return wasRemoved;
    	
  
    }
    
    public Object removeLast()
    {
        /**
         * removes the last element in the linked list
         */
    	Object last;
    	
    	if(getLast() == null) 
    		{
    		return null;
    		}
    	
    	
    	last = this.getLast();
    	remove(getPrevious(null));
    	return last;
    }

    public int size()
    {
    	/**
    	 * return the size of the linked list
    	 */
        return mySize;
    }

    public Object getFirst()
    {
        /**
         * returns first node's data in the linked list
         */
    	
    	if(myHead == null) return null;
    	
    	return myHead.getData();
    	
    }
    
    public Object getLast()
    {
    	/**
    	 * returns the last node's data in the Linked List
    	 */
    	
      if(this.getPrevious(null)==null) return null;
      Object last;
      last = this.getPrevious(null).getData();
      return last;
    }

    public void setFirst(Object o)
    {
    	/**
    	 * sets the data of the head to something new 
    	 */
    	
    	if(myHead == null) 
    	{
    		this.addFirst(o);
   		}
    	
    	myHead.setData(o);
    	
    	
    }
    
    private void setHead(Node node)
    {
    	//sets the head as a node 
        myHead = node;
    }
    
    private Node getHead()
    {
    	//returns the head 
        return myHead;
    }
    
    private class Node
    {
        /**
         * Nodes store data value and and a reference to
         * the next node following it 
         * 
         *   
         * made the class private to be accessed 
         * only by the linked list class
         * 
         */
         
    	
        private Object myData;
        private Node myNext;
        
     
        /**
         *  Default constructor for a node with null
         *  data and pointer to a next node
         */
        public Node()
        {
            myData = null;
            myNext = null;
        }
        
        
        public Node(Object datum)
        {
        	
        	/**
             *  Constructor for a node with some object for
             *  its data and null for a pointer to a next node
             *
             *  <pre>
             *  pre:  a null node
             *  post: a node with some object for its data and
             *        null for a pointer to a next node
             *  </pre>
             *
             *  @param datum an object for the node's data
             */
            myData = datum;
            myNext = null;
        }
        
        
        public Node(Object datum, Node next)
        {
        	
        	/**
             *  Constructor for a node with some object for 
             *  its data and a pointer to another node
             *
             *  <pre>
             *  pre:  a null node
             *  post: a node with some object for its data and
             *        a pointer to a next node
             *  </pre>
             *
             *  @param datum an object for the node's data
             *  @param next the node that this node points to
             */
            myData = datum;
            myNext = next;
        }
        
        
        
        // Accessor methods
        public void setData(Object datum)
        {
            myData = datum;
        }
        
        public Object getData()
        {
            return myData;
        }
        
        public void setNext(Node next)
        {
            myNext = next;
        }
        
        public Node getNext()
        {
            return myNext;
        }
    }
        
}