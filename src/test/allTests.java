package test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ removeTests.class, insertTests.class, containTests.class, genericTests.class, indexTests.class })
public class allTests {

}