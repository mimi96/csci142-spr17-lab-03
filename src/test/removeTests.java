package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import structures.LinkedList;

public class removeTests {

	LinkedList myList;
	
	@Before
	public void setUp() throws Exception 
	{
	 myList = new LinkedList();
	}

	@Test
	public void testIfNodeWasRemoved()
	{
		/**
		 * Test case testing if you can remove node 
		 * from list you added.
		 * 
		 * 1. You need to add a node to the list, which would 
		 * make it the first node now. 
		 * 
		 * 2. Then you remove the same node from the list. 
		 * 
		 * 3. To be correct the node must have been removed and to know that 
		 * you must then contain 0 nodes in the LinkedList as the size of the List 
		 * 
		 * @author maria 
		 */
		
		myList.addFirst("Bailey");
		myList.remove("Bailey");
		boolean check = (!myList.contains("Bailey"));
		
		assertTrue("The node had been successfully removed", check);
	}
	
	
	@Test
	public void testIfYouCanRemoveFromEmptyList()
	{
		/**
		 * This test checks if you are allowed to remove nodes from an 
		 * empty Linked List. 
		 * 
		 * 1. You need to create new Linked List
		 * 
		 * 2. Now the list is empty obviously because you have not yet
		 * put any values in it. 
		 * 
		 * 3. You try to remove a node from the list 
		 * 
		 * 4. Should return false because there is nothing to remove
		 * 
		 */
		
		LinkedList myList = new LinkedList();
		boolean check = myList.remove("Dr.Plante");
		
		assertFalse ("The Linked is empty", check);
	}
	
	@Test 
	public void testIfEmptySize()
	{
		/**
		 *  Tests if Linked List is empty when you make a new one 
		 *  before you add any objects. 
		 *  
		 *  1. Make a new linked list which will now be empty. 
		 *  
		 *  2. Size is 0 
		 *  
		 *  3. Should return true. 
		 */
		
		
		LinkedList myList = new LinkedList();
		Integer size = myList.size();
		boolean check = (size == 0);
		
		assertTrue ("Size is 0", check);
	}
	
	@Test 
	public void testIfElementsAreReallyRemoved()
	{
		/**
		 * tests if elements are really removed 
		 * 
		 */
		
		LinkedList myList = new LinkedList();
		myList.addFirst(1);
		myList.addFirst(2);
		myList.addFirst(3);
		boolean removed = myList.remove(1);
		boolean removedSecond = myList.remove(3);
		boolean check = (myList.contains(2));
		
		assertTrue("only one element left in linked list", check);
		
	}
	
	@Test
	public void testIfNotInListCanBeRemoved()
	{
		/**
		 * Tests if you can remove an object that is not in the Linked List, 
		 * which you should not be able to do. 
		 * 
		 *  1. Make new empty list.
		 *  
		 *  2. Add few objects to the list.
		 *  
		 *  3. Delete an object not in the list from above.
		 *  
		 *  4. Should not work because it does not exist within the LinkedList you have.
		 */
		
		LinkedList myList = new LinkedList();
		myList.addFirst(2);
		myList.addFirst(4);
		myList.addFirst(6);
		myList.addFirst(8);
		boolean notValid = myList.remove(9);
		
		assertFalse("9 is not an object in the Linked List, should return false", notValid);
	}
	
	@Test 
	public void testIfYouCanRemoveNullValues()
	{
		/**
		 * Test if you can remove a null value. Should not be able to if there are not any null values.
		 * If you have a linked List with null values should be able to remove those. 
		 * 
		 * 1. make a new linked list
		 * 
		 * 2. add a value 
		 * 
		 * 3. try to remove a null value 
		 * 
		 * 4. should not work 
		 * 
		 */
		
		LinkedList  myList = new LinkedList();
		myList.addFirst("Richard");
		boolean removeNull = myList.remove(null);
		
		assertFalse("can't remove null", removeNull);
	}
	
	@Test 
	public void testToRemoveLastElement()
	{
		LinkedList list = new LinkedList();
		list.addFirst(1);
		list.addFirst(3);
		list.remove(3);
		boolean validate = list.contains(3);
		
		assertFalse ("should not contain 3", validate);
	}
	
	@Test 
	public void testIfRemovedElementInList()
	{
		/**
		 * testing to see if linked list contains a removed object 
		 * 
		 */
		
		LinkedList myList  = new LinkedList ();
		myList.addFirst("Edward Cullen");
		myList.remove("Edward Cullen");
		boolean check = (myList.contains("Edward Cullen"));
		
		assertFalse("Edward Cullen should have been removed and no longer in list", check);
	}
	
	@Test
	public void testIfRemoveNullThatExist()
	{
		/**
		 * Test to remove a null value that already exists 
		 */
		LinkedList myList = new LinkedList();
		
		myList.addFirst(3);
		myList.addFirst("null");
		boolean removeNull = myList.remove("null");
		boolean check = (myList.contains(3));
		
		assertTrue("Undeeded null value removed", check);
	}

	@Test 
	public void testIfYouCanRemoveSameValueMoreThanOnce()
	{
		/**
		 * test if you can remove a value more than once 
		 */
		
		LinkedList myList = new LinkedList();
		myList.addFirst(1);
		myList.addFirst(4);
		myList.addFirst(3);
		boolean first = myList.remove(3);
		boolean second = myList.remove(3);
		boolean validate = (first == second);
		
		assertFalse("Can't remove twice", validate);
	}
	
	@Test 
	public void testIfRemovingValuesExistTwice()
	{
		/**
		 * test if you can remove a value that exists twice in the linked list. 
		 * 
		 */
		
		LinkedList myList = new LinkedList();
		myList.addFirst("mimi");
		myList.addFirst("kim");
		myList.addFirst(7);
		myList.addFirst("mimi");
		myList.addFirst(4);
		myList.remove("mimi");
		boolean check = (myList.contains("mimi"));
		
		assertTrue ("Should remove value even if there are two instances", check);
	}
	
	@Test 
	public void testRemoveFromSeveral()
	{
		//test to see if you can remove from several 
		myList.addFirst(1);
		myList.addFirst(2);
		myList.addFirst(3);
		myList.addFirst(4);
		myList.remove(3);
		boolean check = myList.contains(3);
		
		assertFalse("3 should be removed", check);
	}
	
	@Test
	public void testRemoveElementCorrect()
	{
		myList.addFirst(1);
		Integer first = myList.size();
		myList.removeFirst();
		Integer last = myList.size();
		
		assertTrue ("size should be 0", last ==0);
	}
	
	@Test
	public void testRemoveDifferentSize()
	{
		myList.addFirst(1);
		Integer first = myList.size();
		myList.removeFirst();
		Integer last = myList.size();
		
		assertTrue ("shoud be different size", last!=first);
	}

}
