package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import structures.LinkedList;

public class containTests {

LinkedList myList;
	
	@Before
	public void setUp() throws Exception 
	{
	 myList = new LinkedList();
	}
	
	@Test
	public void testEmptySize()
	{
		/**
		 * testing if the size of the linked list is empty is correct
		 */
		
		LinkedList list = new LinkedList();
		Integer mySize = list.size();
		boolean validate = mySize==0;
		
		assertTrue("Should be empty", validate);
		
	}
	
	@Test 
	public void testIfSizeIncreased(){
		LinkedList list = new LinkedList();
		list.addFirst("vegan");
		list.addFirst("pizzas");
		list.addFirst("are good");
		Integer validate = list.size();
		
		assertTrue ("Should have a size 3", validate == 3);
	}
	@Test 
	public void testIfSizeAddingIsCorrect()
	{
		LinkedList list = new LinkedList();
		list.addFirst(1);
		Integer f = list.size();
		
		list.addFirst(3);
		Integer l = list.size();
		
		boolean checkSize = f!=l && l == f+1;
		
		assertTrue  ("size should be 2", checkSize);
	}
	
	@Test
	public void testSizeFew()
	{
		myList.addFirst(1);
		myList.addFirst(2);
		myList.addFirst(3);
		Integer size = myList.size();
		
		assertTrue("Size should be 3", size ==3);
		
	}
	@Test
	public void testIfContainsFewAreRemoved()
	{
		LinkedList myList = new LinkedList();
		myList.addFirst(1);
		myList.addFirst("bunny");
		myList.addFirst(2);
		myList.remove(1);
		myList.remove(2);
		boolean validate = (myList.contains(1) && myList.contains(2));
		
		assertFalse("numbers should not be allowed, only bunnies", validate);
	}
	
	@Test 
	public void testIfSizeRemoveOne()
	{
		/**
		 * add from last to first so it appears in order of first to last 
		 */
		myList.addFirst(3);
		myList.addFirst(4);
		Integer first = myList.size();
		myList.removeFirst();
		Integer last = myList.size();
		
		assertTrue("sizes differ", first!=last);
		
	}
	
	@Test 
	public void testIfAddLastWorks()
	{
		LinkedList list = new LinkedList();
		list.addLast("last");
		boolean last = list.contains("last");
		
		assertTrue ("list contains last as last", last);
	}
	
	@Test
	public void testIfItContaintSomethingThatIsNotInList()
	{
		/**
		 * tests if there is something in the list that should 
		 * not be there and which I did not add
		 */
		
		LinkedList myList = new LinkedList();
		myList.addFirst("School is cool");
		myList.addFirst("Sleep is also cool");
		boolean validate = (!myList.contains("decaf is good, said no one"));
		
		assertTrue ("Should not contain decaf mentionings", validate);
	}
	
	@Test 
	public void testIfContaintsOnlyAddedElement()
	{
		/**
		 * Test if it contains only an added element if you have one
		 * 
		 */
		
		LinkedList myList = new LinkedList();
		myList.addFirst("cat");
		boolean check = (myList.contains("cat"));
		
		assertTrue ("contains cat", check);
	}
	
	@Test
	public void testIfSomethingContainedInNullList()
	{
		/**
		 * testing to see if there is something in the linked list that should not be there
		 * if the linked list is empty 
		 */
		
		LinkedList myList = new LinkedList();
		
		assertFalse ("Should not contain name", myList.contains("name"));
	}

}
