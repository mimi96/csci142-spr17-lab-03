package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import structures.LinkedList;

public class genericTests {

LinkedList myList;
	
	@Before
	public void setUp() throws Exception 
	{
	 myList = new LinkedList();
	}
	
	@Test
	public void testIfLinkedListNull()
	{
		/**
		 * testing to see if there is nothing in the linked list 
		 */
		LinkedList myList = new LinkedList();
		
		assertTrue ("should contain nothing", !myList.contains(null));
	}
	
	@Test
	public void testIfLinkedListNotNull()
	{
		/**
		 * testing if the linked list is not null 
		 */
		
		LinkedList list = new LinkedList();
		
		boolean valid = !list.contains(null);
		
		assertTrue("No null in list", valid);
		
	}
	
	@Test 
	public void testIfEmptyListIsTrue()
	{
		LinkedList list = new LinkedList();
		boolean check = (!list.contains(null));
		assertTrue("Should be empty", check);
	}	
	
	
	@Test 
	public void testIfAddingWorks()
	{
		LinkedList myList = new LinkedList();
		myList.addFirst(2);
		boolean validate = myList.contains(2);
		
		assertTrue("adding works, 2 was added ",validate);
		
	}
	@Test 
	public void testSetNone()
	{
		myList.setFirst("coffee");
		assertTrue("first should be coffee", myList.getFirst()=="coffee");
	}
	@Test
	public void testSetFirst()
	{
		Integer node = 3;
		Integer setNewNode = 4;
		
		myList.addFirst(node);
		myList.addFirst(setNewNode);
		
		assertTrue("first should now be 4", myList.getFirst()==setNewNode);
	}
	
	@Test 
	public void testSetSeveralNodes()
	{
		Integer setNew = 100;
		myList.addFirst(1);
		myList.addFirst(2);
		myList.addFirst(3);
		myList.setFirst(setNew);
		
		
		assertTrue("ntht", myList.getFirst()==setNew); 
		
	}
	
	
	

	
	

}
