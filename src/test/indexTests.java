package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import structures.LinkedList;

public class indexTests {

    LinkedList myList;
	
	@Before
	public void setUp() throws Exception 
	{
	 myList = new LinkedList();
	}
	
	
	
	@Test
	public void testIfIndexNone()
	{
		LinkedList mylist = new LinkedList();
		boolean check = mylist.indexOf(null)==-1;
		
		assertTrue("there should be no index num", check);
	}
	
	@Test
	public void testIndexOne()
	{
		/**
		 * test if index of one object list is 0 
		 */
	
		myList.addLast(2);
		boolean index = myList.indexOf(2)==0;
		
		assertTrue("Index is 0 yay", index);
	}
	
	@Test
	public void testIfMoreThanOne()
	{
		myList.addLast(2);
		myList.addFirst(9);
		boolean indexFew = myList.indexOf(1)==1;
		
		assertFalse("index is 1", indexFew);
	}
	
	
	@Test 
	public void testIfIndexIncreased()
	{
		LinkedList mylist = new LinkedList();
		mylist.addLast(2);
		boolean check = mylist.indexOf(2)==0;
		
		assertTrue ("index 0", check);
	}
	
	
	@Test
	public void testIfLinkedListHasMultiple()
	{
		/**
		 * Testing if the linked list has multiple elements in it
		 */
		
		LinkedList mylist = new LinkedList();
		myList.addFirst(1);
		myList.addFirst("Coffee is good");
		myList.addFirst("So are birthdays");
		myList.addFirst(7);
		myList.addFirst(4);
		boolean validate = (myList.contains(1)&&myList.contains(7));
		
		assertTrue("should have 1 and 4", validate);
	}
	
	@Test
	public void testIndexRandomCorrect()
	{
		LinkedList list = new LinkedList();
		list.addFirst(2);
		
		assertTrue ("index must be 1", list.indexOf(2)==0);
	}
	
	@Test
	public void testIfRemoved()
	{
		myList.addFirst(1);
		myList.addFirst(2);
		myList.addFirst(3);
		myList.remove(2);
		boolean check = myList.indexOf(2)== -1;
		
		assertTrue ("index should be -1 because it's below 0", check);
	}
	
	@Test
	public void testIfIndexNotInList()
	{
		myList.addFirst("Joey");
		myList.addFirst("Rachel");
		myList.addFirst("Monica");
		boolean validate = myList.indexOf(2)==-1;
		
		assertTrue("index should be -1", validate);
	}
	
	

}
