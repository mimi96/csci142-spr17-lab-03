package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import structures.LinkedList;

public class insertTests {

	LinkedList myList;
	
	@Before
	public void setUp() throws Exception 
	{
	 myList = new LinkedList();
	}
	
	@Test 
	public void testBeforeEmpty()
	{
		/**
		 *  test if list can insert before an empty linkedList
		 */
		
		myList.insertBefore(1, null);
		
		assertTrue("index should be 0", myList.indexOf(1)==0);
	}
	
	@Test 
	public void testInsertBeforeNull()
	{
		boolean check = myList.insertBefore(4, null);
		
				
		assertTrue ("should be true", check);
	}
	
	@Test 
	public void testInsertBeforeNullGetFirst()
	{
		myList.addFirst(2);
		boolean validate = myList.getFirst().equals(2);
				

		assertTrue ("should be true", validate);
	}

	@Test
	public void testInsertBeforeFirstFew()
	{
		myList.addFirst(2);
		myList.addFirst(6);
		myList.insertBefore(2,1 );
		assertFalse("should have index 2", myList.indexOf(1)==2);
	}
	

	
	@Test
	public void testBeforeTheFirstOne()
	{
		myList.addFirst(3);
		myList.insertBefore(1, 3);
		boolean check = myList.indexOf(1)==1;
		
		assertTrue("should be 1", check);
	}
	

	
}
